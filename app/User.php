<?php
/**
 * Copyright (c) 2020. All Rights Reserved
 * Trébol Colombia SAS - www.trebolcolombia.com
 * Written by Jonathan Torres <jonathan8312 at gmail.com>
 */

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Trebol\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait{
        EntrustUserTrait::restore as entrust_restore;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document', 'name', 'email', 'email_verified_at', 'password', 'mobile', 'address', 'active', 'privacy_policy'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function restore()
    {
        $result = $this->soft_delete_restore();
        $this->entrust_restore();
        return $result;
    }

    public function scopeSearch($query, $search_term)
    {
        $query->where('name', 'like', '%' . $search_term . '%')
            ->orWhere('email', 'like', '%' . $search_term . '%')
            ->orWhere('document', 'like', '%' . $search_term . '%');
    }

    //ESTADO DEL USUARIO
    public function getStatusLabelAttribute()
    {
        switch ($this->active) {
            case true:
                return '<span class="badge badge-outlinesuccess" style="font-size: 100%;"><strong>ACEPTADO</strong></span>';
            case false:
                return '<span class="badge badge-outlinedanger" style="font-size: 100%;"><strong>PENDIENTE</strong></span>';
            default:
                return '<span class="badge badge-outlinedanger" style="font-size: 100%;"><strong>' . $this->public . '</strong></span>';
        }
    }

    //POLITICA DE PRIVACIDAD
    public function getPrivacityLabelAttribute()
    {
        switch ($this->privacy_policy) {
            case true:
                return '<span class="badge badge-outlinesuccess" style="font-size: 100%;"><strong>SI</strong></span>';
            case false:
                return '<span class="badge badge-outlinedanger" style="font-size: 100%;"><strong>NO</strong></span>';
            default:
                return '<span class="badge badge-outlinedanger" style="font-size: 100%;"><strong>' . $this->privacy_policy . '</strong></span>';
        }
    }
}
