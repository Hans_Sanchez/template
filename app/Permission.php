<?php
/**
 * Copyright (c) 2020. All Rights Reserved
 * Trébol Colombia SAS - www.trebolcolombia.com
 * Written by Jonathan Torres <jonathan8312 at gmail.com>
 */

namespace App;


use Trebol\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $fillable = ['name', 'display_name', 'description'];

    public function scopeSearch($query, $search_term)
    {
        $query->where('name', 'like', '%' . $search_term . '%');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permission_role');
    }
}
