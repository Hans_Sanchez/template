<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->active == 1){
            return $next($request);
        }else{
            Auth::logout();
            flash()->warning("<h5 class='text-center justify-content-center'>Su solicitud de registro se está revisando, esta deberá ser aprobada por parte de un/a administrador/a</h5>");
            //\alert()->success("Su solicitud de registro se está revisando, esta deberá ser aprobada por parte de un/a administrador/a");
            return redirect()->route('login');
        }
    }
}
