<?php

namespace App\Http\Middleware;
use App\User;
use Closure;
use Illuminate\Support\Facades\View;

class DrawMenu
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //USUARIOS
        $users_count = User::count();
        $users_active = User::where('active', true)->count();
        $users_pending = User::where('active', false)->count();

        //USUARIOS
        View::share('users_count', $users_count);
        View::share('users_active', $users_active);
        View::share('users_pending', $users_pending);

        return $next($request);
    }
}
