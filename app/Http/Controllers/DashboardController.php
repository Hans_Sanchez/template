<?php
/**
 * Copyright (c) 2020. All Rights Reserved
 * Trébol Colombia SAS - www.trebolcolombia.com
 * Written by Jonathan Torres <jonathan8312 at gmail.com>
 */

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('dashboard');
    }

}
