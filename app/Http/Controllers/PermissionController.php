<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Psy\Util\Str;
use Trebol\Entrust\EntrustPermission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        Paginator::useBootstrap();

        $permissions = Permission::search($request->input('search-permission'))
            ->orderBy('id', 'asc')
            ->paginate(10);

        $roles = Role::all();
        return view('permissions.index', compact('permissions', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'display_name' => 'required',
            'module' => 'required',
        ];

        $messages = [
            'name.required' => 'El nombre es requerido',
            'display_name.required' => 'El nombre en pantalla es requerido',
            'module.required' => 'El módulo al que pertenece es requerido',
        ];

        $request->validate($rules, $messages); //CON ESO OPTIMIZO 1 LINEA
        try {

            DB::beginTransaction();
            $newPermission = new Permission;
            $newPermission->name = \Illuminate\Support\Str::slug($request->input('name'));
            $newPermission->display_name = ucfirst(mb_strtolower($request->input('display_name')));
            $newPermission->module = ucfirst(mb_strtolower($request->input('module')));
            $newPermission->description = $request->input('description');
            $newPermission->save();


            DB::commit();
            flash()->success("El permiso: <b>" . $newPermission->display_name . "</b> se agregó con éxito");
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error("Error: " . $e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('admin.permission-index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $rules = [
            'name' => 'required',
            'display_name' => 'required',
            'module' => 'required',
        ];

        $messages = [
            'name.required' => 'El nombre es requerido',
            'display_name.required' => 'El nombre en pantalla es requerido',
            'module.required' => 'El módulo al que pertenece es requerido',
        ];

        $request->validate($rules, $messages); //CON ESO OPTIMIZO 1 LINEA
        try {

            DB::beginTransaction();
            $permission->name = \Illuminate\Support\Str::slug($request->input('name'));
            $permission->display_name = ucfirst(mb_strtolower($request->input('display_name')));
            $permission->module = ucfirst(mb_strtolower($request->input('module')));
            $permission->description = $request->input('description');
            $permission->update();


            DB::commit();
            flash()->success("El permiso: <b>" . $permission->display_name . "</b> se actualizó con éxito");
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error("Error: " . $e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('admin.permission-index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        try{
            $permission->delete();
            flash()->success("El permiso: <b>{$permission->display_name}</b> se eliminó con éxito");
        }catch(\Exception $e){
            flash()->error("Se ha presentado un error al eliminar el permiso".$e->getMessage());
        }
        return redirect()->route('admin.permission-index');
    }
}
