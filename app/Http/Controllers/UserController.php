<?php

namespace App\Http\Controllers;

use App\Address;
use App\Monitor;
use App\Profile;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;


class UserController extends Controller
{
    private $_module = 'Users';

    public function index(Request $request)
    {
        Paginator::useBootstrap();
        $roles = Role::orderBy('display_name')
            ->get();

        $users = array();
        foreach ($roles as $role) {
            $users[$role->name] = User::search(trim($request->input('search-' . $role->name)))
                ->where(function ($query) {
                    if (Auth::user()->roles->first()->name != 'admin') {
                        $query->where('id', Auth::user()->id);
                    }
                })
                ->whereHas('roles', function ($query) use ($role) {
                    $query->where('id', $role->id);
                })->paginate(10);
        }

        $auth = Auth::user()->id;

        return view('users.index', compact('users', 'roles', 'auth'));
    }

    public function create(Role $role)
    {
        return view('users.create', compact('role'));
    }

    public function store(Request $request, Role $role)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
        ];

        $messages = [
            'email.required' => 'El email es requerido',
            'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
            'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
            'password.required' => 'La contraseña es requerida',
            'password.confirmed' => 'Verifique por favor las contraseñas, no son iguales',
            'password.min' => 'La contraseña debe contener al menos 8 caracteres',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->validate($request->all());
        try {
            DB::beginTransaction();
            $newUser = new User;
            $newUser->name = $request->input('name');
            $newUser->surname = $request->input('surname');
            $newUser->document = $request->input('document');
            $newUser->email = $request->input('email');
            $newUser->password = bcrypt($request->input('password'));
            $newUser->mobile = $request->input('mobile');
            $newUser->price_list_id = $request->input('prices_list');
            $newUser->credit_days = $request->input('credit_days') ?: 0;
            $newUser->active = true;
            $newUser->amount_credit = $request->input('amount_credit') ? str_replace([',', '-', '$', ' ', 'e', 'E'], '', $request->input('amount_credit')) : 0;
            $newUser->save();
            $newUser->roles()->attach($role->id);

            Monitor::create([
                'user_id' => Auth::id(),
                'module' => $this->_module,
                'action' => 'Creación de Usuario',
                'new' => serialize($newUser),
            ]);

            DB::commit();
            flash()->success("El usuario <b>{$newUser->name}</b> con rol <b>{$role->display_name}</b> se agregó con éxito");
            if ($newUser->hasRole('customers')) {
                return redirect()->route('users.profile', $newUser);
            }
            return redirect()->route('users.index');
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error("Error: " . $e->getMessage());
            return redirect()->back()->withInput($request->all());
        }

    }

    public function show(User $user)
    {
        Paginator::useBootstrap();
        $addresses_deleted = Address::onlyTrashed()
            ->whereHas('profile', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
            ->orderByDesc('deleted_at')
            ->paginate(7);

        $back = route('users.index', ['search-' . $user->roles()->first()->name]);

        return view('users.show', compact('user', 'addresses_deleted', 'back'));
    }

    public function edit(User $user)
    {
        $role = $user->roles()->first();
        $roles = Role::get();

        $back = route('users.index', ['search-' . $user->roles()->first()->name]);

        return view('users.edit', compact('user', 'role', 'roles', 'back'));
    }

    public function update(Request $request, User $user)
    {
        if ($request->input('password') != null && $request->input('password_confirmation') != null) {
            $rules = [
                'email' => 'required|email|unique:users,email,' . $user->id,
                'password' => 'required|confirmed|min:8',
            ];

            $messages = [
                'email.required' => 'El email es requerido',
                'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
                'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
                'password.confirmed' => 'Verifique por favor las contraseñas, no son iguales',
                'password.min' => 'La contraseña debe contener al menos 8 caracteres',
            ];
        } else {
            $rules = [
                'email' => 'required|email|unique:users,email,' . $user->id,
            ];

            $messages = [
                'email.required' => 'El email es requerido',
                'email.email' => 'El campo email no tiene un formato adecuado verifique por favor',
                'email.unique' => 'El email que intenta ingresar ya existe, verifique por favor',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->validate($request->all());

        try {
            DB::beginTransaction();

            $monitor = Monitor::create([
                'user_id' => Auth::id(),
                'module' => $this->_module,
                'action' => 'Edición de Usuario',
                'old' => serialize($user),
            ]);

            $password = $user->password;
            $active = $user->active;

            $user->name = $request->input('name');
            $user->surname = $request->input('surname');
            $user->document = $request->input('document');
            $user->email = $request->input('email');
            $user->price_list_id = $request->input('prices_list');

            if ($request->input('password') == null) {
                $user->password = $password;
            } else {
                $user->password = bcrypt($request->input('password'));
            }

            $user->mobile = $request->input('mobile');
            $user->credit_days = $request->input('credit_days') ?: 0;
            $user->amount_credit = $request->input('amount_credit') ? str_replace([',', '-', '$', ' ', 'e', 'E'], '', $request->input('amount_credit')) : 0;

            if ($request->input('status')) {
                $user->active = $request->input('status');
            } else {
                $user->active = $active;
            }

            $user->update();

            $monitor->update([
                'new' => $user
            ]);

            if ($request->input('role')) {
                $user->roles()->sync($request->input('role'));
            } else {
                $user->roles()->sync($user->roles()->first()->id);
            }

            $role = $user->roles()->first();

            DB::commit();
            flash()->success("El usuario <b>{$user->name}</b> con rol <b>{$role->display_name}</b> se actualizó con éxito");
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error("Error: " . $e->getMessage());
            return redirect()->back()->withInput($request->all());
        }
        return redirect()->route('users.index', ['search-' . $role->name]);
    }

    public function destroy(User $user)
    {
        try {
            Monitor::create([
                'user_id' => Auth::id(),
                'module' => $this->_module,
                'action' => 'Eliminación de Usuario',
                'old' => serialize($user->toArray()),
            ]);
            $user->delete();
            flash()->success("El usuario <b>{$user->name}</b> se movió a la papelera con éxito");
        } catch (\Exception $e) {
            flash()->error("Error: " . $e->getMessage());
        }
        return redirect()->route('users.index');
    }

    //Metodos para borrar definitivamente y restaurar
    public function restoreRegister($id)
    {
        try {
            $user = User::withTrashed()->findOrFail($id);
            $user->restore();
            flash()->success("El usuario <b>{$user->name}</b> se restauró con éxito");
        } catch (\Exception $e) {
            flash()->error("Error: " . $e->getMessage());
        }
        return redirect()->route('users.index');
    }

    public function forceDeleted($id)
    {
        try {
            $user = User::withTrashed()->findOrFail($id);
            $user->forceDelete();
            flash()->success("El usuario <b>{$user->name}</b> se eliminó definitivamente con éxito");
        } catch (\Exception $e) {
            flash()->error("Error: " . $e->getMessage());
        }
        return redirect()->route('users.index');
    }

    //Función para traer datos por ajax a un select 2
    public function searchUser(Request $request)
    {
        if ($request->ajax()) {

            $users = User::search($request->input('term'))
                ->where(function ($query) use ($request) {
                    $query->whereHas('roles', function ($query) use ($request) {
                        $query->whereIn('name', ['customer', 'reseller', 'staff', 'admin']);
                    });
                })
                ->orderBy('name')
                ->paginate(10);

            $dataJson = array();

            foreach ($users as $user) {
                $document = ($user->document != null) ? $user->document : 'Sin documento';
                $dataJson['results'][] = [
                    'id' => $user->id,
                    'text' => $user->full_name . ' - (' . $user->email . ') - (' . $document . ')'
                ];
            }
            $dataJson['pagination'] = ['more' => $users->hasMorePages()];

            return response()->json($dataJson);
        }
    }
}
