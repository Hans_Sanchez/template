@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Usuarios</li>
            <li class="breadcrumb-item" aria-current="page">Listado de usuarios</li>
            <li class="breadcrumb-item active" aria-current="page">Editar</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Editar {{$role->display_name}}</strong>
        </div>
        <form action="{{ route('users.update', [$user])}}" method="POST" autocomplete="off">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="row">
                    @if(auth()->user()->hasRole('admin'))
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name"><strong>Nombre <span class="text-danger">*</span></strong></label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{old('name', $user->name)}}" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email"><strong>Correo electrónico <span class="text-danger">*</span></strong></label>
                                <input id="email" class="form-control" type="text" name="email"
                                       data-inputmask="'alias': 'email'" value="{{old('email', $user->email)}}"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="document "><strong>Número de documento <span class="text-danger">*</span></strong></label>
                                <input id="document" class="form-control" type="number" name="document"
                                       value="{{old('document' , $user->document)}}" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mobile"><strong>Dirección <span class="text-danger">*</span></strong></label>
                                <input id="text" class="form-control" type="text" name="address"
                                       value="{{old('address', $user->address)}}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password"><strong>Contraseña</strong></label>
                                <input id="password" class="form-control" type="password" name="password">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password_confirmation"><strong>Confirmar contraseña</strong></label>
                                <input id="password_confirmation" class="form-control" type="password"
                                       name="password_confirmation">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="mobile"><strong>Rol <span class="text-danger">*</span></strong></label>
                                <select name="role" id="role">
                                    @foreach($roles as $role)
                                        <option
                                            value="{{$role->id}}" {{$user->hasRole($role->name)?'selected':''}}>{{$role->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @elseif($user->id == auth()->user()->id)
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="name"><strong>Nombre <span class="text-danger">*</span></strong></label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{old('name', $user->name)}}" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email"><strong>Correo electrónico <span class="text-danger">*</span></strong></label>
                                <input id="email" class="form-control" type="text" name="email"
                                       data-inputmask="'alias': 'email'" value="{{old('email', $user->email)}}"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="document "><strong>Número de documento <span class="text-danger">*</span></strong></label>
                                <input id="document" class="form-control" type="number" name="document"
                                       value="{{old('document' , $user->document)}}" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mobile"><strong>Dirección <span class="text-danger">*</span></strong></label>
                                <input id="text" class="form-control" type="text" name="address"
                                       value="{{old('address', $user->address)}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password"><strong>Contraseña</strong></label>
                                <input id="password" class="form-control" type="password" name="password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password_confirmation"><strong>Confirmar contraseña</strong></label>
                                <input id="password_confirmation" class="form-control" type="password"
                                       name="password_confirmation">
                            </div>
                        </div>
                    @else
                        <div class="row" style="width: 100%;">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    <p>Usted <b>NO</b> puede realizar acciones en un usuario que no es suyo, por favor
                                        regrese al listado de usuarios</p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="{{$back}}" role="button">
                    Volver
                </a>
                @if(auth()->user()->hasRole('admin'))
                    <button type="submit" class="btn btn-success updated">
                        Actualizar
                    </button>
                @elseif($user->id == auth()->user()->id)
                    <button type="submit" class="btn btn-success updated">
                        Actualizar
                    </button>
                @endif
            </div>
        </form>
    </div>
@endsection

