@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Usuarios</li>
            <li class="breadcrumb-item" aria-current="page">Listado de usuarios</li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Nuevo usuario {{$role->display_name}}</strong>
        </div>
        <form action="{{ route('users.store', $role)}}" method="POST" autocomplete="off">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="name"><strong>Nombre <span class="text-danger">*</span>
                                </strong></label>
                            <input id="name" class="form-control" type="text" name="name" required
                                   value="{{old('name')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="email"><strong>Correo electrónico <span class="text-danger">*</span></strong></label>
                            <input id="email" class="form-control" type="text" name="email"
                                   data-inputmask="'alias': 'email'" required value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="document "><strong>Número de documento <span class="text-danger">*</span></strong></label>
                            <input id="document" class="form-control" type="text" name="document"
                                   data-inputmask-alias="9999999999" value="{{old('document')}}" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="mobile"><strong>Dirección <span class="text-danger">*</span></strong></label>
                            <input id="text" class="form-control" type="text" name="address"
                                   value="{{old('address')}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password"><strong>Contraseña <span class="text-danger">*</span></strong></label>
                            <input id="password" class="form-control" type="password" name="password" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password_confirmation"><strong>Confirmar contraseña <span
                                        class="text-danger">*</span></strong></label>
                            <input id="password_confirmation" class="form-control" type="password"
                                   name="password_confirmation" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route ('users.index')}}" role="button">
                    Volver
                </a>
                <button type="submit" class="btn btn-success updated">
                    Guardar
                </button>
            </div>
        </form>
    </div>
@endsection
