@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Usuarios</li>
            <li class="breadcrumb-item active" aria-current="page">Listado de usuarios</li>
        </ol>
    </nav>
    <div class="card text-center">
        <div class="card-header">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @foreach($roles as $role)
                    <li class="nav-item">
                        <a class="nav-link {{count(request()->all()) ? (array_key_exists('search-'.$role->name,request()->all()) ? 'active':''): ($loop->first ? 'active':'')}}"
                           id="{{$role->name}}-tab" data-toggle="tab" href="#{{$role->name}}" role="tab"
                           aria-controls="{{$role->name}}" aria-selected="true">{{$role->display_name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                @foreach($roles as $role)
                    <div
                        class="tab-pane fade {{count(request()->all()) ? (array_key_exists('search-'.$role->name,request()->all()) ? 'show active':''): ($loop->first ? 'show active':'')}}"
                        id="{{$role->name}}" role="tabpanel" aria-labelledby="{{$role->name}}-tab">
                        <br>
                        <div class="row">
                            <div class="col-md-6 text-left">
                                @ability('admin','a-users-create')
                                <a href="{{route('users.create', $role)}}" class="btn btn-outline-primary float-left">
                                    <span class="link-title">
                                        Nuevo usuario
                                    </span>
                                </a>
                                @endability

                            </div>
                            <div class="col-md-6">
                                <form class="form-inline my-2 my-lg-0">
                                    <input type="text" name="search-{{$role->name}}"
                                           value="{{request()->input('search-'.$role->name)}}"
                                           class="form-control mr-sm-2" style="width: 64%" autofocus="autofocus"
                                           placeholder="Ingrese el nombre del {{$role->display_name}} a buscar">
                                    <button class="btn btn-outline-primary">
                                        Buscar
                                    </button>
                                    &nbsp;
                                    <a href="{{route('users.index')}}" class="btn btn-outline-primary">Limpiar</a>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive" id="table-{{$role->name}}">
                            <table id="orders-table" class="table table-bordered table-sm table-hover table-condensed">
                                <thead>
                                <tr class="bg-primary">
                                    <th class="text-center text-white">Número de documento</th>
                                    <th class="text-center text-white">Nombre</th>
                                    <th class="text-center text-white">Correo electrónico</th>
                                    <th class="text-center text-white">Número de teléfono</th>
                                    <th class="text-center text-white">Estado del usuario</th>
                                    @ability('admin','a-users-update,a-users-destroy')
                                    <th class="text-center text-white">Opciones</th>
                                    @endability
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users[$role->name] as $user)
                                    <tr>
                                        <td style="text-align: left !important;">{{$user->document}}</td>
                                        <td style="text-align: left !important;">{{$user->name}}</td>
                                        <td style="text-align: left !important;">{{$user->email}}</td>
                                        <td style="text-align: center !important;">{{$user->mobile}}</td>
                                        <td class="text-center">{!! $user->status_label !!}</td>
                                        @if ($role->name == 'admin')
                                            @ability('admin','a-users-update,a-users-show,a-users-destroy')
                                            <td class="text-center" width="120px">
                                                <div class="btn-group">
                                                    @if(auth()->user()->hasRole('admin'))
                                                        @ability('admin','a-users-update')
                                                        <a href="{{ route ('users.edit', $user) }}"
                                                           title="Ver">
                                                            <i class="link-icon text-warning"
                                                               data-feather="edit-2"></i>
                                                        </a>
                                                        @endability
                                                        &nbsp;
                                                        @ability('admin','a-users-destroy')
                                                        <form method="POST"
                                                              action="{{ route('users.destroy', $user)}}">
                                                            @csrf
                                                            @method('delete')
                                                            <span class="destroy btn-trash"
                                                                  title="Eliminar">
                                                                <i class="link-icon text-danger"
                                                                   data-feather="trash-2"></i></span>
                                                        </form>
                                                        @endability
                                                    @else
                                                        @if($user->id == $auth)
                                                            @ability('admin','a-users-update')
                                                            <a href="{{ route ('users.edit', $user) }}"
                                                               title="Editar">
                                                                <i class="link-icon text-warning"
                                                                   data-feather="edit-2"></i>
                                                            </a>
                                                            @endability
                                                        @endif
                                                    @endif
                                                </div>
                                            </td>
                                            @endability
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            {{$users[$role->name]->appends(request()->all())->render()}}
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection

@push('custom-scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                $(this).parent().find('li .active').removeClass('active');
                $(this).parent('li').addClass('active');
                var $element = $(this).parent().parent().parent();
                if (!$element.prop('id'))
                    $element = $element.parent();

                var page = $(this).prop('href').split('page=')[1];
                var tab = 'search-' + ($element.prop('id').replace('table-', ''));

                console.log($.urlParam(tab));
                if ($.urlParam(tab) == null || $.urlParam(tab) == 0)
                    window.location.href = window.location.href.split('?')[0] + `?${tab}=&page=${page}`;
                else
                    window.location.href = window.location.href.split('?')[0] + `?${tab}=${$.urlParam(tab)}&page=${page}`;
            });

        });


        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            return decodeURI(results[1]) || 0;
        }
    </script>
@endpush
