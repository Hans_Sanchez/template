@extends('layout.master2')

@section('content')
    <div class="page-content d-flex align-items-center justify-content-center">

        <div class="row w-100 mx-0 auth-page">
            <div class="col-md-8 col-xl-6 mx-auto">
                <div class="card">
                    <div class="row">
                        <div class="col-md-4 pr-md-0">
                            <div class="auth-left-wrapper"
                                 style="background-image: url('{{ asset('assets/images/pet-login.jpg') }}')">

                            </div>
                        </div>
                        <div class="col-md-8 pl-md-0">
                            <div class="auth-form-wrapper px-4 py-5">
                                <a href="#" class="noble-ui-logo d-block mb-2">Template.<span>com</span></a>
                                <h5 class="text-muted font-weight-normal mb-4">Por favor ingresa tu correo electrónico
                                y tu nueva contraseña para realizar el cambio</h5>

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Oops!</strong> {{ $errors->first('email') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                @if ($errors->has('password'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Oops!</strong> {{ $errors->first('password') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                <form class="" action="{{ route('password.update') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="form-group">
                                        <label for="email">Correo electrónico</label>
                                        <input type="email" name="email" class="form-control" required id="email"
                                               placeholder="admin@template.pet" value="{{ Request::get('email') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Nueva contraseña</label>
                                        <input type="password" class="form-control" id="password" required
                                               name="password" placeholder="MiClave123" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Confirmar contraseña</label>
                                        <input type="password" class="form-control" id="password_confirmation" required
                                               name="password_confirmation" placeholder="MiClave123">
                                    </div>
                                    <div class="mt-3">
                                        <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0">Cambiar contraseña
                                        </button>
                                        <a href="{{ route('login') }}">Iniciar sesión</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
