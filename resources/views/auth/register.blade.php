@extends('layout.master2')

@section('content')
    <div class="page-content d-flex align-items-center justify-content-center">
        <div class="row w-100 mx-0 auth-page">
            <div class="col-md-8 col-xl-6 mx-auto">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12 pl-5 pr-5">
                            <div class="auth-form-wrapper px-4 py-5">
                                <a href="#" class="noble-ui-logo d-block mb-2">Template.<span>com</span></a>
                                <h5 class="text-muted font-weight-normal mb-4">Registra tus datos totalmente gratis</h5>
                                <form action="{{ route('register') }}" method="post" class="forms-sample"
                                      autocomplete="off">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Nombre
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="name" id="name"
                                               placeholder="Escribe tú nombre aquí" value="{{old('name')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="document">Número de documento
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="document" id="document"
                                               data-inputmask-alias="9999999999"
                                               placeholder="Escribe tú número de documento aquí"
                                               value="{{old('document')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Dirección
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="address" id="address"
                                               placeholder="Ejm. Calle 12 # 135 - 00" value="{{old('address')}}"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Correo electrónico
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="email"
                                               data-inputmask="'alias': 'email'" name="email"
                                               placeholder="Escribe tú correo electrónico aquí"
                                               value="{{old('email')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Contraseña
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control" id="password" name="password"
                                               placeholder="********" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirmación de contraseña
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control" id="password_confirmation"
                                               name="password_confirmation"
                                               placeholder="********" required>
                                    </div>
                                    <div class="mt-3">
                                        <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0">
                                            Registrarme
                                        </button>
                                    </div>
                                    <a href="{{ route('login') }}" class="d-block mt-3">
                                        Regresar a la página de inicio de sesión...
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
