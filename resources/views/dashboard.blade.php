@extends('layout.master')

@push('plugin-styles')
    {!! Html::style('/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') !!}
@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Tablero de control</h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap btn-group">
            <div class="btn-group mb-3 mb-md-0" role="group" aria-label="dashboard-period">
                <a href="{{route('dashboard')}}" role="button"
                   class="btn {{ Ekko::isActiveRoute('dashboard') }} btn-outline-primary">
                    Tablero de control
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <h4 class="text-uppercase">Perfil Profesional</h4>
                                <hr>
                                <div style="font-size: 4em">
                                    0
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- row -->
@endsection

@push('plugin-scripts')
    {!! Html::script('/assets/plugins/chartjs/Chart.min.js') !!}
    {!! Html::script('/assets/plugins/jquery.flot/jquery.flot.js') !!}
    {!! Html::script('/assets/plugins/jquery.flot/jquery.flot.resize.js') !!}
    {!! Html::script('/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('/assets/plugins/apexcharts/apexcharts.min.js') !!}
    {!! Html::script('/assets/plugins/progressbar-js/progressbar.min.js') !!}
@endpush

@push('custom-scripts')
    {!! Html::script('/assets/js/dashboard.js') !!}
    {!! Html::script('/assets/js/datepicker.js') !!}
@endpush
