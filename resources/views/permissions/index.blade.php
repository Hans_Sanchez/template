@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Permisos</li>
            <li class="breadcrumb-item active" aria-current="page">Listado de permisos</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Listado de permisos</strong>
        </div>
        <br>
        <div class="pl-4 pr-4">
            <div class="row">
                <div class="col md-6">
                    @ability('admin','a-permissions-create')
                    <a href="{{route('admin.permission-create')}}" class="btn btn-outline-primary">
                        <span class="link-title">
                            Nuevo
                        </span>
                    </a>
                    @endability
                </div>
                <div class="col-md-6">
                    <form class="form-inline my-2 my-lg-0 float-right">
                        <input class="form-control mr-sm-2" type="text" name="search-permission"
                               value="{{request('search-permission')}}" aria-label="Search">
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="orders-table" class="table table-bordered table-sm table-hover table-condensed">
                    <thead>
                    <tr class="bg-primary">
                        <th class="text-center text-white">Nombre</th>
                        <th class="text-center text-white">Nombre en pantalla</th>
                        <th class="text-center text-white">Módulo</th>
                        <th class="text-center text-white">Descripción</th>
                        {{--@foreach($roles as $role)
                            <th class="text-center text-white" s>{{$role->display_name}}</th>
                        @endforeach--}}
                        @ability('admin','a-permissions-update,a-permissions-destroy')
                        <th class="text-center text-white">Opciones</th>
                        @endability
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($permissions as $permission)
                        <tr>
                            <td width="200px">{{$permission->name}}</td>
                            <td width="180px">{{$permission->display_name}}</td>
                            <td width="180px">{{$permission->module}}</td>
                            <td class="text-justify">{{$permission->description}}</td>
                            {{--@foreach($roles as $role)
                                <td class="text-center justify-content-center" style="margin: 0" width="120px">
                                    <div class="container">
                                        <div class="form-check" style="margin-left: 32px">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="ids[]" id="ids"
                                                       value="{{$permission->id}}">
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            @endforeach--}}
                            @ability('admin','a-permissions-update,a-permissions-destroy')
                            <td class="text-center" width="120px">
                                <div class="btn-group">
                                    @ability('admin','a-permissions-update')
                                    <a href="{{ route ('admin.permission-edit', $permission) }}" title="Editar">
                                        <i class="link-icon text-warning" data-feather="edit-2"></i>
                                    </a>
                                    @endability
                                    &nbsp;
                                    @ability('admin','a-permissions-destroy')
                                    <form method="POST" action="{{ route('admin.permission-destroy', $permission)}}">
                                        @csrf
                                        @method('delete')
                                        <span class="destroy btn-trash" title="Eliminar"><i
                                                class="link-icon text-danger" data-feather="trash-2"></i></span>
                                    </form>
                                    @endability
                                </div>
                            </td>
                            @endability
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                {{$permissions->appends(request()->all())->render()}}
            </div>
        </div>
    </div>
@endsection
