@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('tracing.dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Permisos</li>
            <li class="breadcrumb-item active" aria-current="page">Nuevo</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Nuevo permiso</strong>
        </div>
        <form action="{{ route('admin.permission-store') }}" method="POST" autocomplete="off">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Nombre <span class="text-danger">*</span></strong></label>
                            <input id="name" class="form-control" type="text" value="{{old('name')}}" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="display_name"><strong>Nombre en pantalla <span class="text-danger">*</span></strong></label>
                            <input id="display_name" class="form-control" type="text" value="{{old('display_name')}}" name="display_name" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="module"><strong>Módulo <span class="text-danger">*</span></strong></label>
                            <input id="module" class="form-control" type="text" value="{{old('module')}}" name="module" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description"><strong>Descripción</strong></label>
                            <input id="description" class="form-control" type="text" value="{{old('description')}}" name="description" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route ('admin.permission-index')}}" role="button">
                    Volver
                </a>
                <button type="submit" class="btn btn-success updated">
                    Guardar
                </button>
            </div>
        </form>
    </div>
@endsection
