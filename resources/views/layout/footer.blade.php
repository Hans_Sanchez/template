<footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
  <p class="text-muted text-center text-md-left">Desarrollado por <a href="https://sol-it.com.co" target="_blank">Hans Sánchez</a>. {{now()->format('Y')}}</p>
  <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Hecho con <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
</footer>
