<nav class="sidebar">
    <div class="sidebar-header">
        <a href="{{ route('dashboard') }}" class="sidebar-brand">
            TEMPLATE
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">

            <li class="nav-item {{ Ekko::isActiveRoute(['dashboard', 'today', 'week', 'month', 'year']) }}">
                <a href="{{ url('/') }}" class="nav-link">
                    <i class="link-icon" data-feather="box"></i>
                    <span class="link-title">Tablero de control</span>
                </a>
            </li>

            @ability('admin','a-users-query,a-users-create,a-users-update,a-users-show,a-users-restore,a-users-destroy,
            a-roles-query,a-roles-show,a-roles-update,a-roles-destroy')
            <li class="nav-item nav-category">adminstración</li>
            <li class="nav-item {{ Ekko::areActiveRoutes(['admin.*', 'users.*','roles-*', 'pets.*', 'resellers.*', 'cities.*', 'holidays_days_dates.*']) }}">
                <a class="nav-link" data-toggle="collapse" href="#user" role="button"
                   aria-expanded="{{ is_active_route(['user/*']) }}" aria-controls="user">
                    <i class="link-icon" data-feather="user"></i>
                    <span class="link-title">Administración</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                </a>
                <div
                    class="collapse {{ Ekko::areActiveRoutes(['admin.*', 'users.*',  'roles-*', 'pets.*', 'resellers.*', 'cities.*', 'holidays_days_dates.*'] , 'show') }}"
                    id="user">
                    <ul class="nav sub-menu">

                        @ability('admin','a-users-query,a-users-create,a-users-show,a-users-update,a-users-destroy')
                        <li class="nav-item">
                            <a href="{{ route('users.index')}}"
                               class="nav-link {{Ekko::isActiveRoute(['users.*', 'pets.*'])}}">
                                Usuarios
                            </a>
                        </li>
                        @endability

                        @ability('admin','a-roles-query,a-roles-create,a-roles-show,a-roles-update')
                        <li class="nav-item">
                            <a href="{{route('roles-index')}}"
                               class="nav-link {{Ekko::isActiveRoute('roles-*')}}">
                                Roles
                            </a>
                        </li>
                        @endability

                    </ul>
                </div>
            </li>
            @endability


        </ul>
    </div>
</nav>

