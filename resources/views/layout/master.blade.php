<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ config('app.name') }} @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="_token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    <!-- plugin css -->
{!! Html::style('/assets/fonts/feather-font/css/iconfont.css') !!}
{!! Html::style('/assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
{!! Html::style('/assets/plugins/sweetalert2/sweetalert2.min.css') !!}
{!! Html::style('/assets/plugins/select2/select2.min.css') !!}
{!! Html::style('/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') !!}
{!! Html::style('/assets/plugins/@mdi/css/materialdesignicons.min.css') !!}


<!-- end plugin css -->
@include('vendor.sweetalert.alert')
@stack('plugin-styles')

<!-- common css -->
{!! Html::style('css/app.css') !!}
<!-- end common css -->

    <style>
        span svg {
            cursor: pointer;
        }
    </style>
    @stack('style')
</head>
<body data-base-url="{{url('/')}}" class="sidebar-dark">

{!! Html::script('/assets/js/spinner.js') !!}

<div class="main-wrapper" id="app">
    @include('layout.sidebar')
    <div class="page-wrapper">
        @include('layout.header')
        <div class="page-content">
            @include('flash::message')

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @yield('content')
        </div>
        @include('layout.footer')
    </div>
</div>

<!-- base js -->
{!! Html::script('js/app.js') !!}
{!! Html::script('/assets/plugins/feather-icons/feather.min.js') !!}
{!! Html::script('/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
<!-- end base js -->

<!-- plugin js -->
@stack('plugin-scripts')
<!-- end plugin js -->

<!-- common js -->
{!! Html::script('/assets/js/template.js') !!}
{!! Html::script('js/custom.js') !!}
<!-- end common js -->

{!! Html::script('/assets/plugins/sweetalert2/sweetalert2.min.js') !!}
{!! Html::script('/assets/plugins/select2/select2.min.js') !!}
{!! Html::script('/assets/plugins/select2/js/i18n/es.js') !!}
{!! Html::script('/assets/plugins/inputmask/jquery.inputmask.bundle.min.js') !!}
{!! Html::script('/assets/js/inputmask.js') !!}
{!! Html::script('/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') !!}
{!! Html::script('/assets/js/datepicker.js') !!}
{!! Html::script('/assets/js/autoNumeric.js') !!}
{!! Html::script('/assets/js/jquery.numeric.js') !!}
{!! Html::script('/assets/plugins/typeahead-js/typeahead.bundle.min.js') !!}

<script>
    //$('div.alert').not('.alert-important').delay(8000).fadeOut(350);
    $(document).ready(function () {
        $(location.hash.replace('#', '.')).modal('show');
        location.hash = '';
    });

    $('form').bind("keypress", function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    $(document).on('click', '.modal-pagination .pagination a', function (event) {
        event.preventDefault();
        $(this).parent().find('li .active').removeClass('active');
        $(this).parent('li').addClass('active');
        var $element = $(this).parent().parent().parent().parent().parent().parent().parent();
        if (!$element.prop('id'))
            $element = $element.parent();
        location.href = $(this).prop('href') + "#" + $element.prop('id');

    });


</script>

@stack('custom-scripts')
</body>
</html>
