<!DOCTYPE html>
<html lang="es">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="_token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

    <!-- plugin css -->
    {!! Html::style('/assets/fonts/feather-font/css/iconfont.css') !!}
    {!! Html::style('/assets/plugins/font-awesome/css/font-awesome.min.css') !!}

    {!! Html::style('/assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
    <!-- end plugin css -->
@include('vendor.sweetalert.alert')
@stack('plugin-styles')

<!-- common css -->
{!! Html::style('css/app.css') !!}
<!-- end common css -->

    @stack('style')
    <style type="text/css">
        .page-wrapper {
            background: #b7b7b7 !important;
        }
    </style>
</head>
<body data-base-url="{{url('/')}}">

{!! Html::script('/assets/js/spinner.js') !!}

<div class="main-wrapper" id="app">
    <div class="page-wrapper full-page">
        @include('flash::message')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @yield('content')
    </div>
</div>

<!-- base js -->
{!! Html::script('js/app.js') !!}
{!! Html::script('/assets/plugins/feather-icons/feather.min.js') !!}
<!-- end base js -->

<!-- plugin js -->
@stack('plugin-scripts')
<!-- end plugin js -->

<!-- common js -->
{!! Html::script('/assets/js/template.js') !!}
<!-- end common js -->
<script>
    $('div.alert').not('.alert-important').delay(8000).fadeOut(350);
</script>

@stack('custom-scripts')
</body>
</html>
