@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item active" aria-current="page">Listado de roles</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Listado de roles</strong>
        </div>
        <br>
        <div class="pl-4 pr-4">
            <div class="row">
                <div class="col md-12">
                    @ability('admin','a-roles-create')
                    <a href="{{route('roles-create')}}" class="btn btn-outline-primary">
                        <span class="link-title">
                            Nuevo rol
                        </span>
                    </a>
                    @endability
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="orders-table" class="table table-bordered table-sm table-hover table-condensed">
                    <thead>
                    <tr class="bg-primary">
                        <th class="text-center text-white">Nombre</th>
                        <th class="text-center text-white">Descripción</th>
                        @ability('admin','a-roles-show,a-roles-update')
                        <th class="text-center text-white">Opciones</th>
                        @endability
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($roles as $role)
                        <tr>
                            <td>{{$role->display_name}}</td>
                            <td>{{$role->description?$role->description:'No se registró una descripción para este rol'}}</td>
                            @ability('admin','a-roles-show,a-roles-update')
                            @if($role->display_name == 'Administrador')
                                <td class="text-center" width="120px">
                                    <div class="btn-group">
                                        @ability('admin','a-roles-show')
                                        <a href="{{ route('roles-show', $role)}}" title="Ver">
                                            <i class="link-icon text-info" data-feather="eye"></i>
                                        </a>
                                        @endability
                                    </div>
                                </td>
                            @else
                                <td class="text-center" width="120px">
                                    <div class="btn-group">
                                        @ability('admin','a-roles-show')
                                        <a href="{{ route('roles-show', $role)}}" title="Ver">
                                            <i class="link-icon text-info" data-feather="eye"></i>
                                        </a>
                                        @endability
                                        &nbsp;
                                        @if($role->display_name != 'Administradores')
                                            @ability('admin','a-roles-update')
                                            <a href="{{ route ('roles-edit', $role) }}" title="Editar">
                                                <i class="link-icon text-warning" data-feather="edit-2"></i>
                                            </a>
                                            @endability
                                        @endif
                                        &nbsp;
                                        @if(count($role->permissions) == 0)
                                            @if($role->display_name != 'Administradores')
                                                <form method="POST" action="{{ route('roles-destroy', $role)}}">
                                                    @csrf
                                                    @method('delete')
                                                    <span class="destroy btn-trash" title="Eliminar"><i
                                                            class="link-icon text-danger"
                                                            data-feather="trash-2"></i></span>
                                                </form>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            @endif
                            @endability
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
