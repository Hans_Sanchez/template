@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Tablero de control</a></li>
            <li class="breadcrumb-item" aria-current="page">Administración</li>
            <li class="breadcrumb-item" aria-current="page">Listado de roles</li>
            <li class="breadcrumb-item active" aria-current="page">Detalle</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-header">
            <strong class="text-uppercase">Detalle del {{$role->display_name}}</strong>
        </div>
        <div class="card-body">
            <strong>Descripcción</strong>
            <p>{{$role->description?$role->description:'No se registró una descripción para este rol'}}</p>
            <hr>
            @forelse($permissions as $permission)
                <li>{{$permission->display_name}}</li>
            @empty
                <li>No hay permisos asignados hasta el momento</li>
            @endforelse
        </div>
        <div class="card-footer">
            <a class="btn btn-primary" href="{{ route ('roles-index')}}" role="button">
                Volver
            </a>
        </div>
    </div>
@endsection
