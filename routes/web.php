<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Intervention\Image\ImageManagerStatic as Image;

//Authentication
Route::get('/iniciar-sesion', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/iniciar-sesion', 'Auth\LoginController@login');

// Registration
Route::get('/registro', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/registro', 'Auth\RegisterController@register');

Route::get('/salir', 'Auth\LoginController@logout')->name('logout');

//Reset password
Route::get('recuperar-clave', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('recuperar-clave', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

//User authenticated
Route::group(['middleware' => ['auth']], function () { 

    Route::group(['middleware' => ['userStatus']], function () {

        //dashboard
        Route::get('/', function () {
            return redirect()->route('dashboard');
        });
        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
        });
        +
        //ADMINISTRACIÓN
        Route::group(['prefix' => 'administration'], function () {

            //USUARIOS
            Route::group(['prefix' => 'users'], function () {
                Route::get('/', 'UserController@index')->name('users.index')->middleware('ability:admin,a-users-query|a-users-create|a-users-show|a-users-update|a-users-destroy');
                Route::get('/{role}/create', 'UserController@create')->name('users.create')->middleware('ability:admin,a-users-create');
                Route::post('/{role}/store', 'UserController@store')->name('users.store')->middleware('ability:admin,a-users-create');
                Route::get('/{user}/show', 'UserController@show')->name('users.show')->middleware('ability:admin,a-users-show');
                Route::get('/{user}/edit', 'UserController@edit')->name('users.edit')->middleware('ability:admin,a-users-update');
                Route::put('/{user}/update', 'UserController@update')->name('users.update')->middleware('ability:admin,a-users-update');
                Route::delete('/{user}', 'UserController@destroy')->name('users.destroy')->middleware('ability:admin,a-users-destroy');
            });

            //PERMISOS
            Route::group(['prefix' => 'permissions'], function () {
                Route::get('/', 'PermissionController@index')->name('admin.permission-index')->middleware('ability:admin,a-permissions-query|a-permissions-create|a-permissions-update|a-permissions-destroy');
                Route::get('/create', 'PermissionController@create')->name('admin.permission-create')->middleware('ability:admin,a-permissions-create');
                Route::post('/store', 'PermissionController@store')->name('admin.permission-store')->middleware('ability:admin,a-permissions-create');
                Route::get('/{permission}/edit', 'PermissionController@edit')->name('admin.permission-edit')->middleware('ability:admin,a-permissions-update');
                Route::put('/{permission}/update', 'PermissionController@update')->name('admin.permission-update')->middleware('ability:admin,a-permissions-update');
                Route::delete('/{permission}', 'PermissionController@destroy')->name('admin.permission-destroy')->middleware('ability:admin,a-permissions-destroy');
            });

            //ROLES
            Route::group(['prefix' => 'roles'], function () {
                Route::get('/', 'RoleController@index')->name('roles-index')->middleware('ability:admin,a-roles-query|a-roles-create|a-roles-update|a-roles-show');
                Route::get('/create', 'RoleController@create')->name('roles-create')->middleware('ability:admin,a-roles-create');
                Route::post('/store', 'RoleController@store')->name('roles-store')->middleware('ability:admin,a-roles-create');
                Route::get('/{role}/show', 'RoleController@show')->name('roles-show')->middleware('ability:admin,a-roles-show');
                Route::get('/{role}/edit', 'RoleController@edit')->name('roles-edit')->middleware('ability:admin,a-roles-update');
                Route::put('/{role}/update', 'RoleController@update')->name('roles-update')->middleware('ability:admin,a-roles-update');
                Route::delete('/{role}', 'RoleController@destroy')->name('roles-destroy')->middleware('ability:admin,a-roles-destroy');
            });

        });

        //LOGS
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    });
});


Route::group(['prefix' => 'email'], function () {
    Route::get('inbox', function () {
        return view('pages.email.inbox');
    });
    Route::get('read', function () {
        return view('pages.email.read');
    });
    Route::get('compose', function () {
        return view('pages.email.compose');
    });
});

Route::group(['prefix' => 'apps'], function () {
    Route::get('chat', function () {
        return view('pages.apps.chat');
    });
    Route::get('calendar', function () {
        return view('pages.apps.calendar');
    });
});

Route::group(['prefix' => 'ui-components'], function () {
    Route::get('alerts', function () {
        return view('pages.ui-components.alerts');
    });
    Route::get('badges', function () {
        return view('pages.ui-components.badges');
    });
    Route::get('breadcrumbs', function () {
        return view('pages.ui-components.breadcrumbs');
    });
    Route::get('buttons', function () {
        return view('pages.ui-components.buttons');
    });
    Route::get('button-group', function () {
        return view('pages.ui-components.button-group');
    });
    Route::get('cards', function () {
        return view('pages.ui-components.cards');
    });
    Route::get('carousel', function () {
        return view('pages.ui-components.carousel');
    });
    Route::get('collapse', function () {
        return view('pages.ui-components.collapse');
    });
    Route::get('dropdowns', function () {
        return view('pages.ui-components.dropdowns');
    });
    Route::get('list-group', function () {
        return view('pages.ui-components.list-group');
    });
    Route::get('media-object', function () {
        return view('pages.ui-components.media-object');
    });
    Route::get('modal', function () {
        return view('pages.ui-components.modal');
    });
    Route::get('navs', function () {
        return view('pages.ui-components.navs');
    });
    Route::get('navbar', function () {
        return view('pages.ui-components.navbar');
    });
    Route::get('pagination', function () {
        return view('pages.ui-components.pagination');
    });
    Route::get('popovers', function () {
        return view('pages.ui-components.popovers');
    });
    Route::get('progress', function () {
        return view('pages.ui-components.progress');
    });
    Route::get('scrollbar', function () {
        return view('pages.ui-components.scrollbar');
    });
    Route::get('scrollspy', function () {
        return view('pages.ui-components.scrollspy');
    });
    Route::get('spinners', function () {
        return view('pages.ui-components.spinners');
    });
    Route::get('tabs', function () {
        return view('pages.ui-components.tabs');
    });
    Route::get('tooltips', function () {
        return view('pages.ui-components.tooltips');
    });
});

Route::group(['prefix' => 'advanced-ui'], function () {
    Route::get('cropper', function () {
        return view('pages.advanced-ui.cropper');
    });
    Route::get('owl-carousel', function () {
        return view('pages.advanced-ui.owl-carousel');
    });
    Route::get('sweet-alert', function () {
        return view('pages.advanced-ui.sweet-alert');
    });
});

Route::group(['prefix' => 'forms'], function () {
    Route::get('basic-elements', function () {
        return view('pages.forms.basic-elements');
    });
    Route::get('advanced-elements', function () {
        return view('pages.forms.advanced-elements');
    });
    Route::get('editors', function () {
        return view('pages.forms.editors');
    });
    Route::get('wizard', function () {
        return view('pages.forms.wizard');
    });
});

Route::group(['prefix' => 'charts'], function () {
    Route::get('apex', function () {
        return view('pages.charts.apex');
    });
    Route::get('chartjs', function () {
        return view('pages.charts.chartjs');
    });
    Route::get('flot', function () {
        return view('pages.charts.flot');
    });
    Route::get('morrisjs', function () {
        return view('pages.charts.morrisjs');
    });
    Route::get('peity', function () {
        return view('pages.charts.peity');
    });
    Route::get('sparkline', function () {
        return view('pages.charts.sparkline');
    });
});

Route::group(['prefix' => 'tables'], function () {
    Route::get('basic-tables', function () {
        return view('pages.tables.basic-tables');
    });
    Route::get('data-table', function () {
        return view('pages.tables.data-table');
    });
});

Route::group(['prefix' => 'icons'], function () {
    Route::get('feather-icons', function () {
        return view('pages.icons.feather-icons');
    });
    Route::get('flag-icons', function () {
        return view('pages.icons.flag-icons');
    });
    Route::get('mdi-icons', function () {
        return view('pages.icons.mdi-icons');
    });
});

Route::group(['prefix' => 'general'], function () {
    Route::get('blank-page', function () {
        return view('pages.general.blank-page');
    });
    Route::get('faq', function () {
        return view('pages.general.faq');
    });
    Route::get('invoice', function () {
        return view('pages.general.invoice');
    });
    Route::get('profile', function () {
        return view('pages.general.profile');
    });
    Route::get('pricing', function () {
        return view('pages.general.pricing');
    });
    Route::get('timeline', function () {
        return view('pages.general.timeline');
    });
});

Route::group(['prefix' => 'auth'], function () {
    Route::get('login', function () {
        return view('pages.auth.login');
    });
    Route::get('register', function () {
        return view('pages.auth.register');
    });
});


Route::group(['prefix' => 'error'], function () {
    Route::get('404', function () {
        return view('pages.error.404');
    });
    Route::get('500', function () {
        return view('pages.error.500');
    });
});


Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

// 404 for undefined routes
Route::any('/{page?}', function () {
    return View::make('pages.error.404');
})->where('page', '.*');
