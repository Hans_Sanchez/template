<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('roles')->delete();

        \DB::table('roles')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'Administradores',
                'description' => 'Control total de la aplicación, no requiere que se le especifiquen los permisos',
                'created_at' => now(),
                'updated_at' => now(),
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'customer',
                'display_name' => 'Clientes',
                'description' => 'Tiene un acceso limitado a dichas funciones en la aplicación las cuales son:',
                'created_at' => now(),
                'updated_at' => now(),
            ),
        ));


    }
}
