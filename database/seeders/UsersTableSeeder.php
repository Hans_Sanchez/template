<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'document' => '1069766258',
                'name' => 'Hans Yadiel Sánchez Mora',
                'email' => 'hanssanchez427@gmail.com',
                'email_verified_at' => NULL,
                'password' => bcrypt('Hans123++'),
                'mobile' => '3126248950',
                'active' => 1,
                'privacy_policy' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ),
        ));

    }
}
