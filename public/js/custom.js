'use strict';
//  Author: AdminDesigns.com
//
//  This file is reserved for changes made by the use.
//  Always seperate your work from the theme. It makes
//  modifications, and future theme updates much easier
//
/*
$(".money").maskMoney({
    prefix: '$ ',
    suffix: "",
    formatOnBlur: false,
    allowZero: true,
    allowNegative: false,
    allowEmpty: true,
    thousands: '.',
    decimal: ',',
    precision: 0

});*/
(function($) {
    $(".modal form.send").on('submit', function (element) {
        element.preventDefault();
        var url = $(this).attr( 'action' );
        var method = $(this).attr( 'method' );
        var refresh = $(this).hasClass('reload');
        $(this).parent().parent().parent().modal('hide');
        Swal.fire({
            title: 'Cargando',
            showCloseButton: true,
            onBeforeOpen: () => {
                Swal.fire.showLoading()
            }
        });

        var form = $(this)[0];
        $.ajax({
            method: method,
            url: url,
            data: $(this).serialize(),
            success: function (json) {
                if ($trigger){
                    $trigger.append(json.option);
                    $trigger.change();
                }
                Swal.fire({
                    title: json.title,
                    icon: 'success',
                    text: json.body,
                    showCloseButton: true,
                    focusConfirm: true,
                    timer: 5000,
                    onClose: () => {
                        form.reset();
                        if (refresh)
                            location.reload();
                    }
                });
            },

            error: function (status) {
                switch (status.status) {
                    case 400:
                        var json = JSON.parse(status.responseText);
                        var html = '';
                        $.each(json, function (index, value) {
                            html = html + '<strong>' + value[0] + '</strong><br>'
                        });

                        Swal.fire({
                            title: '¡Error!',
                            icon: 'error',
                            html: html,
                            showCloseButton: true,
                            focusConfirm: true
                        })
                        break;
                    default:
                        Swal.fire({
                            title: '¡Error!',
                            icon: 'error',
                            text: status.responseJSON,
                            showCloseButton: true,
                            focusConfirm: true
                        })
                }
            }

        });

    });

    $('.btn-cancel').on('click', function(event){
        event.preventDefault();
        var link = $(this).attr('href');

        Swal.fire({
            title: "¡Atención!",
            text: "Al salir, el sistema no guardará los cambios que haya podido realizar",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText:"Sí, deseo cancelar",
            cancelButtonText: "Volver",
        }).then(function(salir){
            if(salir.value){
                window.location.href = link;
            }
        });

    });

    //boton para la enviar a la papelera
    $('.btn-trash').on('click', function(event){
        event.preventDefault();
        var form =  ($(this).parent());
        Swal.fire({
            title: "¡Atención!",
            text: "¡Una vez borre el registro lo podrá encontrar en la papelera!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, enviar a la papelera",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });

    //boton para restaurar de la papelera
    $('.btn-restore').on('click', function(event){
        event.preventDefault();
        var form =  ($(this).parent());
        Swal.fire({
            title: "¡Atención!",
            text: "¿Desea restaurar este registro?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo restaurarlo",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });

    $('.btn-delete').on('click', function(event){
        event.preventDefault();
        var form =  ($(this).parent());
        Swal.fire({
            title: "¡Atención!",
            text: "¡Una vez borrado el registro no se podrá recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo borrarlo",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });


    $('.btn-finish').on('click', function(event){
        event.preventDefault();
        var form =  ($(this).parent().parent().parent());

        Swal.fire({
            title: "¡Atención!",
            text: "¡Una vez finalizada la orden no se podrá modificar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo finalizarla",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });

    $('.btn-abort').on('click', function(event){
        event.preventDefault();
        var form =  ($(this).parent());
        Swal.fire({
            title: "¡Atención!",
            text: "¡Una vez anulada la orden no se podrá recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo anularla",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });

    $('.btn-confirm').on('click', function(event){
        event.preventDefault();
        var form =  ($(this).parent().parent());
        Swal.fire({
            title: "¡Atención!",
            text: "¿Está seguro de realizar la acción?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo continuar",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });

    $('.btn-deletes').on('click', function(event){
        event.preventDefault();
        var id = $(this).attr('id');
        var form =  $('.' + id);

        Swal.fire({
            title: "¡Atención!",
            text: "¡Una vez borrado el registro no se podrá recuperar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Sí, deseo borrarlo",
            cancelButtonText: "Volver",
        }).then(function(borrar){
            if(borrar.value){
                form.submit();
            }else{
                return false;
            }
        });

    });

})(jQuery);

function number_format(amount, decimals) {
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
    decimals = decimals || 0; // por si la variable no fue fue pasada
    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);
    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}

function str_replace (search, replace, subject, countObj) {

    var i = 0
    var j = 0
    var temp = ''
    var repl = ''
    var sl = 0
    var fl = 0
    var f = [].concat(search)
    var r = [].concat(replace)
    var s = subject
    var ra = Object.prototype.toString.call(r) === '[object Array]'
    var sa = Object.prototype.toString.call(s) === '[object Array]'
    s = [].concat(s)

    var $global = (typeof window !== 'undefined' ? window : global)
    $global.$locutus = $global.$locutus || {}
    var $locutus = $global.$locutus
    $locutus.php = $locutus.php || {}

    if (typeof (search) === 'object' && typeof (replace) === 'string') {
        temp = replace
        replace = []
        for (i = 0; i < search.length; i += 1) {
            replace[i] = temp
        }
        temp = ''
        r = [].concat(replace)
        ra = Object.prototype.toString.call(r) === '[object Array]'
    }

    if (typeof countObj !== 'undefined') {
        countObj.value = 0
    }

    for (i = 0, sl = s.length; i < sl; i++) {
        if (s[i] === '') {
            continue
        }
        for (j = 0, fl = f.length; j < fl; j++) {
            temp = s[i] + ''
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0]
            s[i] = (temp).split(f[j]).join(repl)
            if (typeof countObj !== 'undefined') {
                countObj.value += ((temp.split(f[j])).length - 1)
            }
        }
    }
    return sa ? s : s[0]
}

